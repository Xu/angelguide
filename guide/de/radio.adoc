== icon:broadcast-tower[size=fw]  Funk ==

Neben den DECT-Telefonen werden auch Handfunkgeräte zur Kommunikation eingesetzt. Sie dienen als Backup an wichtigen Positionen bei Ausfall des DECT-Netzes und werden auch für die 1-n-Kommunikation eingesetzt.

Unter Umständen bekommst du in einer Schicht ein Funkgerät. Achte in diesem Fall darauf, dass der Akku nicht leer ist, der richtige Kanal eingestellt ist und das Gerät laut genug ist, damit du es verstehst. Die Kommunikation beginnt immer mit einer Initialisierungsphase mit Rufzeichen. Da wahrscheinlich niemand weiß, dass du der Engel vor der Eingangstür bist, solltest Du "Eingang Glashalle" als Rufzeichen verwenden, nicht deinen richtigen Namen oder deinen Nicknamen. Zum Beispiel: "Secu für Eingang Glashalle" - "Hier ist Secu, bitte sprich".

Weitere Informationen zum Funkverkehr findest du unter https://howto.c3roc.de.

