== icon:fast-forward[size=fw]  Quick Start

[square]
. Create yourself an https://engelsystem.de[angel system account]
. Arrive at congress venue
. Locate and move to https://c3nav.de[heaven]
. Get marked as arrived and collect your badge
  * Ask a welcome angel or shift coordinator
. Attend an angel meeting
  * Announced in the angel system news
. Click yourself an interesting shift
  * Read shift descriptions first
. Participate in your shift
  * Use https://c3nav.de[navigation] to find location
  * Arrive a little bit early
. Rest for at least one hour
. Repeat from step 6

And always, **have a lot of fun**.
