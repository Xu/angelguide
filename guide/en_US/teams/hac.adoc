=== HAC [small]#(Hall Angel Coordination)#

If you have already visited a talk in a lecture hall and have been led into the hall by the angels and guided to a vacant seat, you have already got to know the work of the HAC.

Angels who work in a big lecture hall, managing the flow of creatures in and out of the hall during and between the talks, are called _Hall Angels_. The _Hall Angel Coordinator_ is responsible for the coordination of the hall angels in the lecture halls. Together they ensure that the event is carried out in a smooth and safe manner.
