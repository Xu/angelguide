=== Heaven

The heaven-team are the angels who work as shift coordinators in the heaven and organise all the work that has to be done and the helping angels in the Engelsystem. They are excellent problem solvers who are attending the congress for several years, so whenever you have a problem during your shift or have questions regarding your angel work don't hesitate to call heaven or come by and ask for help.
