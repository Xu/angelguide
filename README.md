# Angel Guide for Chaos Events

This guide is written in Asciidoc.

## Requirements
* asciidoctor
* asciidoctor-pdf

## Asciidoctor resources

[Quick Reference](https://asciidoctor.org/docs/asciidoc-syntax-quick-reference/)
[User Manual](https://asciidoctor.org/docs/user-manual/)

## How to build the PDF

Run the following command in the git repositories root folder, after installing
the requirements.

```
  $ asciidoctor-pdf guide.adoc
```

Build the guide in other languages:
```
  $ asciidoctor-pdf -a lang=de guide.adoc
```
